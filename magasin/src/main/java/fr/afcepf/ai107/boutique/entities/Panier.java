package fr.afcepf.ai107.boutique.entities;

import java.util.ArrayList;

/**
 * Respr�sente le panier d'un utilisateur.
 */
public class Panier {

    /**
     * Default constructor.
     */
    public Panier() {
        lignes = new ArrayList<PanierLigne>();
    }

    /**
     * Lignes constituant le panier.
     */
    private ArrayList<PanierLigne> lignes;

    /**
     * Calcule le montant total du panier.
     * @return somme des montants des lignes
     */
    public float getMontantTotal() {
        float total = 0;
        for (PanierLigne ligne : this.lignes) {
            total += ligne.getMontant();
        }
        return total;
    }

    /**
     * Ajoute un article au panier avec la quantit� sp�cifi�e.
     * Si le panier contient d�j� cette r�f�rence, la quantit� est ajout�e � la ligne correspondante.
     * @param article article � ajouter
     * @param quantite quantit� � ajouter
     */
    public void ajouter(Article article, int quantite) {
        boolean found = false;

        // d�terminer si l'article est d�j� pr�sent dans le panier
        for (PanierLigne ligne : lignes) {
            // article trouv� : mise � jour de la quantit� :
            if (ligne.getArticle().getRef().equals(article.getRef())) {
                found = true;
                ligne.setQuantite(ligne.getQuantite() + quantite);
                break;
            }
        }
        
        // article non trouv� : ajout d'une nouvelle ligne :
        if (!found) {
            PanierLigne nouvelleLigne = new PanierLigne(quantite, article);
            lignes.add(nouvelleLigne);
        }
    }
    
    /**
     * Nombres de lignes dans le panier
     * @return
     */
    public int getNbLignes() {
        return lignes.size();
    }
    
    /**
     * Nombre total d'articles dans le panier
     * @return
     */
    public int getNbArticles() {
        int total = 0;
        
        for(PanierLigne ligne : lignes)
        {
            total += ligne.getQuantite();
        }
        
        return total;
    }
    
}