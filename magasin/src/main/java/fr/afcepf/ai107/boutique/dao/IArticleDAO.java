package fr.afcepf.ai107.boutique.dao;

import java.util.List;

import fr.afcepf.ai107.boutique.entities.Article;

/**
 * Interface g�n�rique pour le DAO Article.
 */
public interface IArticleDAO {

    /**
     * @return tous les articles.
     */
    List<Article> getAll();

    /**
     * @param ref
     * @return article dont la r�f�rence correspond � ref, null sinon
     */
    Article getByRef(String ref);

    /**
     * Mise � jour du stock de l'article demand�.
     * @param idArticle id de l'article concern�
     * @param quantite nouvelle quantit�
     */
    void updateStock(int idArticle, int quantite);

}