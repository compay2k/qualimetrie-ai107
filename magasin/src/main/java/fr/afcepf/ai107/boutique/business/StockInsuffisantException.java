/**
 * 
 */
package fr.afcepf.ai107.boutique.business;

import fr.afcepf.ai107.boutique.entities.Article;

/**
 * @author bchauvet
 *
 */
public class StockInsuffisantException extends Exception {

    private Article article;
    private int quantiteDemandee;
    
    private final static String MESSAGE = "Stock insuffisant";
    
    /**
     * @param message
     */
    public StockInsuffisantException(Article article, int quantiteDemandee) {
        super(MESSAGE);
        this.article = article;
        this.quantiteDemandee = quantiteDemandee;
    }

    /**
     * @return the article
     */
    public Article getArticle() {
        return article;
    }

    /**
     * @return the quantiteDemandee
     */
    public int getQuantiteDemandee() {
        return quantiteDemandee;
    }
    
    

}
