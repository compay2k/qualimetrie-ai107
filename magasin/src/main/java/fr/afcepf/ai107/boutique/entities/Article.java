package fr.afcepf.ai107.boutique.entities;

/**
 * Entit� d�crivant un Article.
 */
public class Article {

    /**
     * Default constructor.
     */
    public Article() {
    }

    /**
     * @param id
     * @param ref
     * @param prixUnitaire
     * @param stock
     */
    public Article(int id, String ref, float prixUnitaire, int stock) {
        this.id = id;
        this.ref = ref;
        this.prixUnitaire = prixUnitaire;
        this.stock = stock;
    }

    /**
     * identifiant de l'article.
     */
    private int id;

    /**
     * reference de l'article.
     */
    private String ref;

    /**
     * Prix unitaire de l'article en euros.
     */
    private float prixUnitaire;

    /**
     * Quantit� en stock.
     */
    private int stock;


    /**
     * @return id
     */
    public int getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return ref
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * @param ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * @return prixUnitaire
     */
    public float getPrixUnitaire() {
        return prixUnitaire;
    }

    /**
     * @param prixUnitaire
     */
    public void setPrixUnitaire(float prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    /**
     * @return strock
     */
    public int getStock() {
        return stock;
    }

    /**
     * @param stock
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

}