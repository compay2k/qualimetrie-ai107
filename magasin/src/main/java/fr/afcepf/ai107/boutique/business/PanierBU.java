package fr.afcepf.ai107.boutique.business;

import fr.afcepf.ai107.boutique.dao.IArticleDAO;
import fr.afcepf.ai107.boutique.entities.Article;
import fr.afcepf.ai107.boutique.entities.Panier;

/**
 * Logique m�tier d'un panier bien rempli tout court.
 */
public class PanierBU {

    /**
     * Default constructor.
     * @param articleDAO dao � utiliser
     */
    public PanierBU(IArticleDAO articleDAO) {
        this.articleDAO = articleDAO;
    }

    /**
     * DAO � utiliser pour les articles.
     */
    private IArticleDAO articleDAO;

    /**
     * Si le stock est suffisant, ajoute l'article au panier avec la quantit� demand�e, puis met � jour le stock.
     * @param ref
     * @param quantite
     * @param panier
     * @throws StockInsuffisantException 
     */
    public void ajouterAuPanier(String ref, int quantite, Panier panier) throws StockInsuffisantException {
        // r�cup�rer l'article recherch� :
        Article articleAAjouter = articleDAO.getByRef(ref);
        
        // si stock suffisant :
        if (articleAAjouter.getStock() >= quantite) {
            // ajout au panier :
            panier.ajouter(articleAAjouter, quantite);
            // mise � jour du stock :
            int nouveauStock = articleAAjouter.getStock() - quantite;
            articleDAO.updateStock(articleAAjouter.getId(), nouveauStock);
        }
        else
        {
            throw new StockInsuffisantException(articleAAjouter, quantite);
        }
    }


}