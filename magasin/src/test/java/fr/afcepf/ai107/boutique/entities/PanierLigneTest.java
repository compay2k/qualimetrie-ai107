package fr.afcepf.ai107.boutique.entities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PanierLigneTest {

    @Test
    public void testGetMontant()
    {
        float prixUnitaire = 43.99f;
        Article a = new Article(1, "abc", prixUnitaire, 2);
        int quantite = 10;
        float montantAttendu = 439.9f;
        
        PanierLigne pl = new PanierLigne(quantite, a);
        
        assertEquals(montantAttendu, pl.getMontant(), 0.1);
    }
    
    @Test
    public void testGetMontantQuantiteZero()
    {
        float prixUnitaire = 43.99f;
        Article a = new Article(1, "abc", prixUnitaire, 2);
        int quantite = 0;
        float montantAttendu = 0;
        
        PanierLigne pl = new PanierLigne(quantite, a);
        
        assertEquals(montantAttendu, pl.getMontant(), 0.1);
    }
    
    @Test
    public void testGetMontantPrixUnitaireZero()
    {
        float prixUnitaire = 0f;
        Article a = new Article(1, "abc", prixUnitaire, 2);
        int quantite = 10;
        float montantAttendu = 0;
        
        PanierLigne pl = new PanierLigne(quantite, a);
        
        assertEquals(montantAttendu, pl.getMontant(), 0.1);
    }
    
}
