package fr.afcepf.ai107.boutique.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import fr.afcepf.ai107.boutique.entities.Article;

/**
 * @author bchauvet
 *
 */
public class ArticleDAOMock implements IArticleDAO {

    /**
     *  pour stocker les donn�es en dutr dans le mock.
     */
    private List<Article> articlesData;

    /**
     * constructeur.
     */
    public ArticleDAOMock() {
        articlesData = new ArrayList<Article>();

        articlesData.add(new Article(1, "pct", 12.5f, 42));
        articlesData.add(new Article(2, "tnd", 97f, 10));
        articlesData.add(new Article(3, "vad", 24f, 100));
        articlesData.add(new Article(4, "cro", 12.5f, 42));
    }

    public List<Article> getAll() {
        return articlesData;
    }

    /**
     * c pas bien.
     * @param ref
     * @return Article
     */
    public Article getByRef(String ref) {
        Article result = null;

        for (Article a : articlesData) {
            if (a.getRef().equals(ref)) {
                result = a;
            }
        }

        return result;
    }

    /**
     * MAJ Stock.
     * @param idArticle
     * @param quantite
     */
    public void updateStock(int idArticle, int quantite) {
        for (Article a : articlesData) {
            if (a.getId() == idArticle) {
                a.setStock(quantite);
            }
        }
    }

}
