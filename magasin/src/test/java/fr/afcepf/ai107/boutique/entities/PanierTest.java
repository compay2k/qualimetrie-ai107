package fr.afcepf.ai107.boutique.entities;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PanierTest {

    private Panier panier;
    private Article article1;
    private Article article2;
    
    @Before
    public void setUp()
    {
        panier = new Panier();
        article1 = new Article(1, "abc", 30.45f, 100);
        article2 = new Article(2, "def", 11.55f, 100);
    }
    
    @Test
    public void testerAjoutArticle()
    {
        int quantite = 5;
        panier.ajouter(article1, quantite);
        
        int nbLignesAttendu = 1;
        int quantiteAttendue = 5;
        
        assertEquals("premierAjout nb lignes KO", nbLignesAttendu, panier.getNbLignes());
        assertEquals("premierAjout quantite KO", quantiteAttendue, panier.getNbArticles());
       
        quantite = 3;
        panier.ajouter(article2, quantite);
        
        nbLignesAttendu = 2; // 1 + 1
        quantiteAttendue = 8; // 5 + 3
        
        assertEquals("deuxiemeAjout nb lignes KO", nbLignesAttendu, panier.getNbLignes());
        assertEquals("deuxiemeAjout quantite KO", quantiteAttendue, panier.getNbArticles());
        
        quantite = 10;
        panier.ajouter(article1, quantite);  
        
        nbLignesAttendu = 2; // 1 + 1
        quantiteAttendue = 18; // 5 + 3 + 10
        
        assertEquals("ajout doublon nb lignes KO", nbLignesAttendu, panier.getNbLignes());
        assertEquals("ajout doublon quantite KO", quantiteAttendue, panier.getNbArticles());
    
    }
    
    @Test
    public void testGetMontant()
    {
        assertEquals("erreur montant panier vide", 0, panier.getMontantTotal(), 0.0f);
    
        panier.ajouter(article1, 5);
        panier.ajouter(article2, 10);
        
        float montantAttendu = (30.45f * 5) + (11.55f * 10);
        assertEquals("erreur montant attendu", montantAttendu, panier.getMontantTotal(), 0.1f);
    }
    
}

    
    
    
    
    
    
    
    