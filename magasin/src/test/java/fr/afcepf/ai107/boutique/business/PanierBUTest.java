package fr.afcepf.ai107.boutique.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.afcepf.ai107.boutique.dao.ArticleDAOMock;
import fr.afcepf.ai107.boutique.dao.IArticleDAO;
import fr.afcepf.ai107.boutique.entities.Article;
import fr.afcepf.ai107.boutique.entities.Panier;

public class PanierBUTest {

    private PanierBU bu;
    private Panier panier;
    private IArticleDAO mock;
    
    @Before
    public void setUp()
    {
        mock = new ArticleDAOMock();
        bu = new PanierBU(mock);
        panier = new Panier();
    }
    
    @Test(expected = StockInsuffisantException.class)
    public void testAjoutPanierStockInsuffisant() throws StockInsuffisantException
    {
        String refArticle = "tnd"; // en stock
        
        bu.ajouterAuPanier(refArticle, 14, panier);
        
    }
    
    @Test
    public void testAjoutPanier() throws StockInsuffisantException
    {
        String refArticle = "pct";
        
        bu.ajouterAuPanier(refArticle, 10, panier);
                
        // verifier le contenu du panier
        float montantAttendu = 125f;
        
        assertTrue(panier.getMontantTotal() == montantAttendu);
        
        // verifier stock de l'article "pct"
        int stockAttendu = 32;
        Article pct = mock.getByRef(refArticle);
                
        assertEquals(stockAttendu, pct.getStock());
    }
    
}
